/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Implementation of the EvaluationWidget class
** File name: evaluationwidget.cpp
**
****************************************************************/

#include <QHBoxLayout>
#include <QVBoxLayout>

#include "evaluationwidget.h"

EvaluationWidget::EvaluationWidget(int row, int type, QList<QChar> charList,
    QList<int> mistakeList, QWidget* parent)
    : QWidget(parent)
    , tabEvaluation(new QTabWidget(this))
    , companyLogo(new CompanyLogo(this))
    , charTableSql(new CharTableSql(this))
    , progressionWidget(new ProgressionWidget(this))
    , fingerWidget(new FingerWidget(this))
    , currentRow(row)
{
    // Object of the user's lesson table
    lessonTableSql = new LessonTableSql(row, type, charList, mistakeList, this);

    // Object of the comparison chart widget
    QWidget* comparisonContainer = new QWidget();
    comparisonWidget = new QTextBrowser(comparisonContainer);
    QVBoxLayout* comparisonLayout = new QVBoxLayout;
    comparisonLayout->addWidget(comparisonWidget);

    comparisonContainer->setLayout(comparisonLayout);

    createComparisonTable();

    if (row > 0) {
        lessonResult = new LessonResult(row, charList, mistakeList, this);
        tabEvaluation->addTab(lessonResult, tr("Report"));
    }

    // Add three tabs and insert the table objects
    tabEvaluation->addTab(lessonTableSql, tr("Overview of Lessons"));
    tabEvaluation->addTab(progressionWidget, tr("Progress of Lessons"));
    tabEvaluation->addTab(charTableSql, tr("Characters"));
    tabEvaluation->addTab(fingerWidget, tr("Fingers"));
    tabEvaluation->addTab(comparisonContainer, tr("Comparison Table"));

    // Create exit (ready) button
    createButtons();

    // Set the layout of all widgets above
    createLayout();

    // Button connection to private slot clickReady()
    connect(buttonReady, &QPushButton::clicked, this,
        &EvaluationWidget::clickReady);
    connect(
        buttonHelp, &QPushButton::clicked, this, &EvaluationWidget::showHelp);

    // Set focus on button
    // buttonReady->setFocus();
}

void EvaluationWidget::createButtons()
{
    // Default button "Ready"
    buttonHelp = new QPushButton(tr("&Help"));
    buttonReady = new QPushButton(tr("&Close"));
    buttonReady->setDefault(true);
}

void EvaluationWidget::createLayout()
{
    // Button layout horizontal
    QHBoxLayout* buttonLayoutHorizontal = new QHBoxLayout;
    buttonLayoutHorizontal->addWidget(buttonHelp);
    buttonLayoutHorizontal->addWidget(buttonReady);
    // Button layout vertical
    QVBoxLayout* buttonLayoutVertical = new QVBoxLayout;
    buttonLayoutVertical->addSpacing(20);
    buttonLayoutVertical->addLayout(buttonLayoutHorizontal);
    // Logo layout vertical
    QVBoxLayout* logoLayout = new QVBoxLayout;
    logoLayout->addSpacing(10);
    logoLayout->addWidget(companyLogo);
    // Logo layout + button layout horizontal
    QHBoxLayout* bottomLayout = new QHBoxLayout;
    bottomLayout->addLayout(logoLayout);
    bottomLayout->addStretch(1);
    bottomLayout->addLayout(buttonLayoutVertical);
    // Full layout of all widgets vertical
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(tabEvaluation);
    mainLayout->addSpacing(1);
    mainLayout->addLayout(bottomLayout);
    mainLayout->setSpacing(15);
    // Pass layout to parent widget (this)
    this->setLayout(mainLayout);
}

void EvaluationWidget::clickReady()
{
    // User finished the evaluation
    // -> emit a ready signal
    emit readyClicked();
}

void EvaluationWidget::showHelp()
{
    QString ahref = "";
    int tab = tabEvaluation->currentIndex();
    if (currentRow <= 0) {
        tab++;
    }
    switch (tab) {
    case 0:
        ahref = "#registerreport";
        break;
    case 1:
        ahref = "#registerlessons";
        break;
    case 2:
        ahref = "#registerprogression";
        break;
    case 3:
        ahref = "#registerchars";
        break;
    case 4:
        ahref = "#fingerrates";
        break;
    case 5:
        ahref = "#registercomparison";
        break;
    }
    helpBrowser = new HelpBrowser("results.html" + ahref, nullptr);
    helpBrowser->show();
}

void EvaluationWidget::createComparisonTable()
{

    QString content;

    content = ""
              "<div "
              "style=\"margin-top:12px;margin-left:16px;margin-bottom:12px;"
              "margin-right:16px;\">"
              "<div style=\"margin-top:16px;font-size:12px;\">"
              "&nbsp;<br><b>"
        + tr("Use examples")
        + "</b>"
          "</div>"
          "<div style=\"margin-top:10px;font-size:11px;\">"
        + tr("Please note that you get better scores for slow typing "
             "without errors, than for fast typing with lots of errors!")
        + "</div>"
          "<div style=\"margin-top:8px;font-size:10px;\">"
          "<table width=\"100%\" border=\"0.5\"cellspacing=\"0\" "
          "cellpadding=\"1\" "
          "style=\"border-style:solid;\">"
          "<tr>"
          "<td>"
          "<b>"
        + tr("Score")
        + "</b>"
          "</td>"
          "<td>"
          "<b>"
        + tr("For example, this equates to …")
        + "</b>"
          "</td>"
          "<td>"
          "<b>"
        + tr("Performance")
        + "</b>"
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "0 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(20).arg(16).arg(5)
        + ""
          "</td>"
          "<td rowspan=\"3\" valign=\"bottom\">"
          ""
        + tr("No experience in touch typing")
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "5 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(20).arg(2).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "8 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(20).arg(0).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "9 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(30).arg(2).arg(5)
        + ""
          "</td>"
          "<td rowspan=\"3\" valign=\"bottom\">"
          ""
        + tr("First steps in touch typing")
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "13 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(40).arg(2).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "16 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(40).arg(0).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "17 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(50).arg(2).arg(5)
        + ""
          "</td>"
          "<td rowspan=\"3\" valign=\"bottom\">"
          ""
        + tr("Advanced level")
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "25 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(70).arg(2).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "32 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(80).arg(0).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "35 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(100).arg(3).arg(5)
        + ""
          "</td>"
          "<td rowspan=\"3\" valign=\"bottom\">"
          ""
        + tr("Suitable skills")
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "37 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(100).arg(2).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "45 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(120).arg(2).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "48 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(120).arg(0).arg(5)
        + ""
          "</td>"
          "<td rowspan=\"3\" valign=\"bottom\">"
          ""
        + tr("Very good skills")
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "53 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(140).arg(2).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "69 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(180).arg(2).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "77 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(200).arg(2).arg(5)
        + ""
          "</td>"
          "<td rowspan=\"3\" valign=\"bottom\">"
          ""
        + tr("Perfect skills")
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "86 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(200).arg(1).arg(5)
        + ""
          "</td>"
          "</tr>"
          "<tr>"
          "<td>"
          "110 "
        + tr("Points")
        + ""
          "</td>"
          "<td>"
        + tr("%1 cpm and %2 errors in %3 minutes").arg(220).arg(1).arg(5)
        + ""
          "</td>"
          "</tr>"
          "</table>"
          "<br>&nbsp;"
          "</div>"
          "</div>";

    comparisonWidget->viewport()->setAutoFillBackground(false);
    comparisonWidget->setHtml(content);
}
