/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Implementation of the HelpBrowser class
** File name: helpbrowser.cpp
**
****************************************************************/

#include <QApplication>
#include <QDebug>
#include <QHBoxLayout>
#include <QPrintDialog>
#include <QPrinter>
#include <QVBoxLayout>

#include "def/defines.h"
#include "helpbrowser.h"
#include <datadir/datadirutils.h>

HelpBrowser::HelpBrowser(QString link, QWidget* parent)
    : QDialog(parent)
    , textBrowser(new QTextBrowser(this))
{
    setWindowFlags(windowFlags() ^ Qt::WindowContextHelpButtonHint);

    setWindowTitle(tr("Help"));
    // setModal(false);

    // Create buttons
    createButtons();

    textBrowser->setOpenExternalLinks(true);

    QString rooturl(QString("file:///") + DataDirUtils::getDataDir());

    textBrowser->setSource(
        rooturl + QString("help/") + tr("en") + QString("/index.html"));

    if (link != "") {
        textBrowser->setSource(rooturl + QString("help/") + tr("en")
            + QString("/content/") + link);
    }

    // Set the layout of all widgets created above
    createLayout();

    // Widget connections
    connect(buttonClose, &QPushButton::clicked, this, &HelpBrowser::clickClose);
    connect(buttonBack, &QPushButton::clicked, textBrowser,
        &QTextBrowser::backward);
    connect(
        buttonHome, &QPushButton::clicked, textBrowser, &QTextBrowser::home);
    connect(buttonPrint, &QPushButton::clicked, this, &HelpBrowser::clickPrint);
    connect(textBrowser, &QTextBrowser::sourceChanged, this,
        &HelpBrowser::changePage);
    connect(textBrowser, &QTextBrowser::backwardAvailable, buttonBack,
        &QWidget::setEnabled);

    buttonClose->setFocus();

    resize(790, 570);
}

void HelpBrowser::createButtons()
{
    // Buttons
    buttonBack
        = new QPushButton(QIcon(":/img/help_arrow_left.png"), tr("Back"));
    buttonBack->setEnabled(false);
    buttonHome = new QPushButton(
        QIcon(":/img/help_home.png"), tr("Table of Contents"));
    buttonClose = new QPushButton(tr("&Close"));
    buttonClose->setDefault(true);
    buttonPrint
        = new QPushButton(QIcon(":/img/help_print.png"), tr("&Print page"));
}

void HelpBrowser::createLayout()
{
    // Button layout horizontal
    QHBoxLayout* buttonLayoutTop = new QHBoxLayout;
    buttonLayoutTop->addWidget(buttonBack);
    buttonLayoutTop->addWidget(buttonHome);
    buttonLayoutTop->addStretch(1);
    buttonLayoutTop->addWidget(buttonPrint);
    // Center layout horizontal
    QHBoxLayout* layoutHorizontal = new QHBoxLayout;
    layoutHorizontal->addSpacing(1);
    layoutHorizontal->addWidget(textBrowser);
    layoutHorizontal->addSpacing(1);
    // Button layout horizontal
    QHBoxLayout* buttonLayoutBottom = new QHBoxLayout;
    buttonLayoutBottom->addStretch(1);
    buttonLayoutBottom->addWidget(buttonClose);
    // Full layout of all widgets vertical
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(buttonLayoutTop);
    mainLayout->addLayout(layoutHorizontal);
    mainLayout->addSpacing(1);
    mainLayout->addLayout(buttonLayoutBottom);
    mainLayout->setSpacing(15);
    // Pass layout to parent widget (this)
    this->setLayout(mainLayout);
}

void HelpBrowser::clickClose() { accept(); }

void HelpBrowser::clickPrint()
{

    QPrinter printer;

    QPrintDialog* dialog = new QPrintDialog(&printer, this);
    dialog->setWindowTitle(tr("Print page"));

    if (dialog->exec() != QDialog::Accepted)
        return;

    textBrowser->print(&printer);
}

void HelpBrowser::changePage(QUrl url)
{
    qDebug() << "changePage Function, parameter: " << url;
    // this->setWindowTitle(url.toString());
}
