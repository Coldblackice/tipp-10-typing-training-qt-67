/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Definition of the ErrorMessage class
** File name: errormessage.h
**
****************************************************************/

#ifndef ERRORMESSAGE_H
#define ERRORMESSAGE_H

#include <QString>
#include <QWidget>

// Error message text
enum class Error {
    // Failed pic operations error numbers
    logo_pic = 101,
    key_pic = 102,
    ticker_pic = 103,
    status_pic = 104,

    // SQL connection error numbers
    sql_db = 201,
    sql_connection = 202,
    sql_db_app_exist = 203,
    sql_db_user_exist = 204,
    sql_db_app_copy = 205,
    sql_db_user_copy = 206,

    // Lesson operation error numbers
    db_version_exist = 301,
    user_lessons_flush = 302,
    user_errors_flush = 303,
    lessons_exist = 304,
    lessons_selected = 305,
    lessons_creation = 306,
    lessons_update = 307,
    user_errors_refresh = 308,
    user_lessons_refresh = 309,
    user_lesson_add = 310,
    user_lesson_get = 311,
    user_lesson_analyze = 312,
    user_import_read = 313,
    user_import_empty = 314,
    user_download_execution = 315,
    user_export_write = 316,

    // Update operations error numbers
    temp_file_creation = 401,
    update_execution = 402,
    online_version_readable = 403,
    db_version_readable = 404,
    update_sql_execution = 405,
    error_defines_exist = 406,
    lesson_content_exist = 407,
    analyze_table_creation = 408,
    analyze_index_creation = 409,
    analyze_table_fill = 410
};

//! The ErrorMessage class provides an error message.
/*!
        The ErrorMessage class puts different parameters together to a full
   error message.

        @author Tom Thielicke, s712715
        @version 0.0.3
        @date 02.07.2006
*/
class ErrorMessage : public QWidget {
    Q_OBJECT

public:
    // Error message type numbers
    enum class Type { Info, Warning, Critical };
    // Error message cancel numbers
    enum class Cancel { No, Operation, Update, Program };

    //! Empty constructor.
    /*!
            @param parent The parent QWidget
    */
    ErrorMessage(QWidget* parent = 0);

    //! Shows a message window
    /*!
            This function puts different parameters together using the
            functions getCancelText() and getErrorText. After that ist shows
            the message on the screen.

            @param errorNo Number of the error
            @param errorType Message type
            @param cancelProcedure Type of an additional cancel text
            @param addon Free text to apend it on the message text
            @see getCancelText(), getErrorText()
    */
    void showMessage(Error errorNo, Type errorType, Cancel cancelProcedure,
        QString addon = "");

private:
    //! Selects the corresponding cancel text of the cancel text number
    /*!
            @param type Type of the cancel text
            @return Corresponding cancel text
    */
    QString getCancelText(Cancel type);

    //! Selects the corresponding error text of the error text number
    /*!
            @param number Number of the error text
            @return Corresponding error text
    */
    QString getErrorText(Error number);
};

#endif // ERRORMESSAGE_H
