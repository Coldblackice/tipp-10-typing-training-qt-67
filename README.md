# Tipp10 ( Qt 6.7 ) - unofficial

This is a fork ( Qt 6.7 ) of the Tipp10 version from https://gitlab.com/tipp10/tipp10

Patches are welcome.

## Installation

### From Source
```sh
git clone https://gitlab.com/philipp-michelfeit/tipp10.git
cd tipp10
mkdir build
cd build
cmake .. / cmake-gui ..
make    # or ninja etc. # command depends on the selected CMake generator ( or use Visual Studio on Windows )
```
