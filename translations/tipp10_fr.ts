<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AbcRainWidget</name>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="80"/>
        <source>Press space bar to start</source>
        <translation>Appuyez sur la touche Espace pour commencer</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="107"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="109"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="112"/>
        <source>E&amp;xit Game</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="364"/>
        <source>Number of points:</source>
        <translation>Nombre de points:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="431"/>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="432"/>
        <source>Points:</source>
        <translation>Points:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="443"/>
        <source>Press space bar to proceed</source>
        <translation>Tapez Espace pour démarrer</translation>
    </message>
</context>
<context>
    <name>CharSqlModel</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="60"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
</context>
<context>
    <name>CharTableSql</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="90"/>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="91"/>
        <source>Target Errors</source>
        <translation>Erreurs</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="92"/>
        <source>Actual Errors</source>
        <translation>Typo</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="93"/>
        <source>Frequency</source>
        <translation>Fréquence</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="94"/>
        <source>Error Rate</source>
        <translation>Taux d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="97"/>
        <source>This column shows all of the
characters typed</source>
        <translation>Cette colonne affiche tous les caractères tapés</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="101"/>
        <source>The character was supposed to be typed, but wasn&apos;t</source>
        <translation>Le caractère était censé être tapé, mais ne l&apos;était pas</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="104"/>
        <source>Character was mistyped</source>
        <translation>Le caractère a été mal saisi</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="106"/>
        <source>This column indicates the total frequency of each
character shown</source>
        <translation>Cette colonne indique la fréquence totale de chaque
caractère affiché</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="110"/>
        <source>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</source>
        <translation>Le taux d&apos;erreurs indique les caractères
qui vous posent le plus de problèmes.
C&apos;est le rapport entre le nombre d&apos;erreurs et
la fréquence d&apos;apparition du caractère dans la dictée.</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="134"/>
        <source>Reset characters</source>
        <translation>Réinitialiser les caractères</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="207"/>
        <source>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</source>
        <translation>Les taux d&apos;erreur enregistrés affectent la fonction d&apos;intelligence et la sélection du texte à dicter. Si le taux d&apos;erreur pour un certain caractère est excessivement élevé, il peut être utile de réinitialiser la liste.

Tous les caractères enregistrés seront maintenant supprimés.

Souhaitez-vous toujours continuer ?</translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <location filename="../widget/errormessage.cpp" line="82"/>
        <source>The process will be aborted.</source>
        <translation>L&apos;opération sera annulée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="85"/>
        <source>The update will be aborted.</source>
        <translation>La mise à jour sera annulée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="88"/>
        <source>The program will be aborted.</source>
        <translation>Le programme sera arrêté.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="99"/>
        <source>Cannot load the program logo.</source>
        <translation>Erreur au chargement du logo.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="102"/>
        <source>Cannot load the keyboard bitmap.</source>
        <translation>Impossible de charger l&apos;image du clavier.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="105"/>
        <source>Cannot load the timer background.</source>
        <translation>Impossible de charger l&apos;arrière-plan du minuteur.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="108"/>
        <source>Cannot load the status bar background.</source>
        <translation>Impossible de charger le fond de l&apos;image de la barre de statut.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="113"/>
        <source>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</source>
        <translation>Impossible de trouver la base de données %1. Importation du fichier impossible.
Veuillez vérifier qu&apos;il s&apos;agit bien d&apos;un fichier texte.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="118"/>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</source>
        <translation>Impossible de trouver la base de données dans le répertoire indiqué.
TIPP10 va créer une nouvelle base de données dans ce répertoire.

Vous pouvez changer le chemin vers la base de données dans les réglages du programme.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="124"/>
        <source>Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</source>
        <translation>Impossible de créer la base de données dans votre répertoire personnel. Vous n&apos;avez peut-être pas les droits d&apos;écriture.
TIPP10 va utiliser la base de données originale placée dans le répertoire du programme.

Vous pourrez changer son emplacement dans les réglages du programme.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="131"/>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</source>
        <translation>Impossible de créer la base de données à l&apos;emplacement indiqué. Le répertoire n&apos;existe pas ou vous n&apos;avez pas les droits d&apos;écriture.
TIPP10 va tenter de la créer dans votre répertoire personnel.

Vous pourrez changer son emplacement dans les réglages du programme.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="138"/>
        <location filename="../widget/errormessage.cpp" line="142"/>
        <source>Connection to the database failed.</source>
        <translation>La connexion à la base de données a échoué.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="145"/>
        <source>The user table with lesson data  cannot be emptied.
SQL statement failed.</source>
        <translation>La table des leçons personnelles n&apos;a pu être vidée.
La requête SQL a échouée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="149"/>
        <source>The user table with error data cannot be emptied.
SQL statement failed.</source>
        <translation>La table des erreurs n&apos;a pu être vidée.
La requête SQL a échouée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="153"/>
        <source>No lessons exist.</source>
        <translation>Il n&apos;y a pas de leçon.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="156"/>
        <source>No lesson selected.
Please select a lesson.</source>
        <translation>Veuillez sélectionner une leçon.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="159"/>
        <source>Cannot create the lesson.
SQL statement failed.</source>
        <translation>Impossible de créer la leçon.
La requête SQL a échoué.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="163"/>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>Les données de la leçon personnelle n&apos;ont pu être sauvegardées
dans la base de données.

Si ce problème apparaît après avoir noté un ralentissement de votre ordinateur,
il se peut que la base de données ait été endommagée (par un crash de l&apos;ordinateur par ex.).
Pour vérifier cela, vous pouvez renommer le fichier contenant la base de données et 
redémarrer le logiciel (il créera automatiquement une nouvelle base de données).

Vous trouverez le chemin de la base de données &quot;%1&quot; dans les réglages.

Si ce problème se produit au premier démarrage de l&apos;application,
vérifiez que la base de données est bien accessible en lecture et en écriture.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="178"/>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>Les statistiques d&apos;erreurs de frappe n&apos;ont pu être sauvegardées
dans la base de données.

Si ce problème apparaît après avoir noté un ralentissement de votre ordinateur,
il se peut que la base de données ait été endommagée (par un crash de l&apos;ordinateur par ex.).
Pour vérifier cela, vous pouvez renommer le fichier contenant la base de données et 
redémarrer le logiciel (il créera automatiquement une nouvelle base de données).

Vous trouverez le chemin de la base de données &quot;%1&quot; dans les réglages.

Si ce problème se produit au premier démarrage de l&apos;application,
vérifiez que la base de données est bien accessible en lecture et en écriture.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="193"/>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>Les données de la leçon personnelle n&apos;ont pu être sauvegardées
dans la base de données.

Si ce problème apparaît après avoir noté un ralentissement de votre ordinateur,
il se peut que la base de données ait été endommagée (par un crash de l&apos;ordinateur par ex.).
Pour vérifier cela, vous pouvez renommer le fichier contenant la base de données et 
redémarrer le logiciel (il créera automatiquement une nouvelle base de données).

Vous trouverez le chemin de la base de données &quot;%1&quot; dans les réglages.

Si ce problème se produit au premier démarrage de l&apos;application,
vérifiez que la base de données est bien accessible en lecture et en écriture.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="207"/>
        <source>Cannot save the lesson.
SQL statement failed.</source>
        <translation>Impossible de sauvegarder les données de la leçon personnelle.
La requête SQL a échouée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="210"/>
        <source>Cannot retrieve the lesson.
SQL statement failed.</source>
        <translation>Impossible de récupérer la leçon.
Échec de l&apos;instruction SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="213"/>
        <source>Cannot analyze the lesson.
SQL statement failed.</source>
        <translation>Impossible d&apos;analyser la leçon.
Échec de l&apos;instruction SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="216"/>
        <source>The file could not be imported.
Please check whether it is a readable text file.
</source>
        <translation>Importation du fichier impossible.
Veuillez vérifier qu&apos;il s&apos;agit bien d&apos;un fichier texte.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="221"/>
        <source>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</source>
        <translation>Importation du fichier impossible : il est vide.
Veuillez indiquer un fichier texte non vide.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="226"/>
        <source>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</source>
        <translation>Importation du fichier impossible.
Vérifiez que l&apos;adresse Internet indiquée pointe bien vers un fichier texte.
Veuillez vérifier aussi votre connexion Internet.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="231"/>
        <source>The file could not be exported.
Please check to see whether it is a writable text file.
</source>
        <translation>Impossible d&apos;exporter le fichier.
Veuillez vérifier qu&apos;il s&apos;agit bien d&apos;un fichier texte modifiable.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="235"/>
        <source>Cannot create temporary file.</source>
        <translation>Impossible de créer de fichier temporaire.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="238"/>
        <source>Cannot execute the update process.
Please check your internet connection and proxy settings.</source>
        <translation>Impossible d&apos;effectuer la mise à jour.
Vérifiez votre connexion Internet et les paramètres de votre proxy si vous en utilisez un.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="242"/>
        <source>Cannot read the online update version.</source>
        <translation>Impossible de lire la version du programme sur Internet.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="245"/>
        <source>Cannot read the database update version.</source>
        <translation>Impossible de lire la version de la base de données.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="248"/>
        <source>Cannot execute the SQL statement.</source>
        <translation>Impossible d&apos;exécuter la requête SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="251"/>
        <source>Cannot find typing mistake definitions.</source>
        <translation>Aucune définition d&apos;erreur de saisie n&apos;a été trouvé.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="254"/>
        <source>Cannot create temporary file.
Update failed.</source>
        <translation>Impossible de créer de fichier temporaire.
La mise à jour a échoué.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="257"/>
        <source>Cannot create analysis table.</source>
        <translation>Impossible de créer le tableau d&apos;analyse.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="260"/>
        <source>Cannot create analysis index.</source>
        <translation>Impossible de créer l&apos;index d&apos;analyse.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="263"/>
        <source>Cannot fill analysis table with values.</source>
        <translation>Impossible de remplir le tableau d&apos;analyse avec des valeurs.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="267"/>
        <source>An error has occured.</source>
        <translation>Une erreur s&apos;est produite.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="271"/>
        <source>
(Error number: %1)
</source>
        <translation>
(Erreur numéro : %1)
</translation>
    </message>
</context>
<context>
    <name>EvaluationWidget</name>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="62"/>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="66"/>
        <source>Overview of Lessons</source>
        <translation>Leçons effectuées</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="67"/>
        <source>Progress of Lessons</source>
        <translation>Progression</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="68"/>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="69"/>
        <source>Fingers</source>
        <translation>Doigts</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="70"/>
        <source>Comparison Table</source>
        <translation>Table de comparaison</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="89"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="90"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="172"/>
        <source>Use examples</source>
        <translation>Utiliser des exemples</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="176"/>
        <source>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</source>
        <translation>Veuillez noter que vous obtenez de meilleurs scores pour une frappe lente sans erreurs, que pour une frappe rapide avec beaucoup d&apos;erreurs !</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="186"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="191"/>
        <source>For example, this equates to …</source>
        <translation>Par exemple, cela équivaut à …</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="196"/>
        <source>Performance</source>
        <translation>Performance</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="203"/>
        <location filename="../widget/evaluationwidget.cpp" line="219"/>
        <location filename="../widget/evaluationwidget.cpp" line="230"/>
        <location filename="../widget/evaluationwidget.cpp" line="241"/>
        <location filename="../widget/evaluationwidget.cpp" line="257"/>
        <location filename="../widget/evaluationwidget.cpp" line="268"/>
        <location filename="../widget/evaluationwidget.cpp" line="279"/>
        <location filename="../widget/evaluationwidget.cpp" line="295"/>
        <location filename="../widget/evaluationwidget.cpp" line="306"/>
        <location filename="../widget/evaluationwidget.cpp" line="317"/>
        <location filename="../widget/evaluationwidget.cpp" line="333"/>
        <location filename="../widget/evaluationwidget.cpp" line="344"/>
        <location filename="../widget/evaluationwidget.cpp" line="355"/>
        <location filename="../widget/evaluationwidget.cpp" line="371"/>
        <location filename="../widget/evaluationwidget.cpp" line="382"/>
        <location filename="../widget/evaluationwidget.cpp" line="393"/>
        <location filename="../widget/evaluationwidget.cpp" line="409"/>
        <location filename="../widget/evaluationwidget.cpp" line="420"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="207"/>
        <location filename="../widget/evaluationwidget.cpp" line="223"/>
        <location filename="../widget/evaluationwidget.cpp" line="234"/>
        <location filename="../widget/evaluationwidget.cpp" line="245"/>
        <location filename="../widget/evaluationwidget.cpp" line="261"/>
        <location filename="../widget/evaluationwidget.cpp" line="272"/>
        <location filename="../widget/evaluationwidget.cpp" line="283"/>
        <location filename="../widget/evaluationwidget.cpp" line="299"/>
        <location filename="../widget/evaluationwidget.cpp" line="310"/>
        <location filename="../widget/evaluationwidget.cpp" line="321"/>
        <location filename="../widget/evaluationwidget.cpp" line="337"/>
        <location filename="../widget/evaluationwidget.cpp" line="348"/>
        <location filename="../widget/evaluationwidget.cpp" line="359"/>
        <location filename="../widget/evaluationwidget.cpp" line="375"/>
        <location filename="../widget/evaluationwidget.cpp" line="386"/>
        <location filename="../widget/evaluationwidget.cpp" line="397"/>
        <location filename="../widget/evaluationwidget.cpp" line="413"/>
        <location filename="../widget/evaluationwidget.cpp" line="424"/>
        <source>%1 cpm and %2 errors in %3 minutes</source>
        <translation>%1 cpm et %2 erreurs en %3 minutes</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="212"/>
        <source>No experience in touch typing</source>
        <translation>Aucune expérience en dactylographie</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="250"/>
        <source>First steps in touch typing</source>
        <translation>Premiers pas dans la dactylographie</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="288"/>
        <source>Advanced level</source>
        <translation>Niveau avancé</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="326"/>
        <source>Suitable skills</source>
        <translation>Compétences adaptées</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="364"/>
        <source>Very good skills</source>
        <translation>Très bonnes compétences</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="402"/>
        <source>Perfect skills</source>
        <translation>Compétences parfaites</translation>
    </message>
</context>
<context>
    <name>FingerWidget</name>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="244"/>
        <source>Error rates of your fingers</source>
        <translation>Taux d&apos;erreur de vos doigts</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="251"/>
        <source>The error rate is based on the recorded characters and the current selected keyboard layout.</source>
        <translation>Le taux d&apos;erreur est basé sur les caractères enregistrés et la disposition de clavier actuellement sélectionnée.</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="261"/>
        <source>Error Rate:</source>
        <translation>Taux d&apos;erreur:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="263"/>
        <source>Frequency:</source>
        <translation>Fréquence:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="265"/>
        <source>Errors:</source>
        <translation>Erreurs :</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="43"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="56"/>
        <location filename="../widget/helpbrowser.cpp" line="59"/>
        <source>en</source>
        <translation>fr</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="85"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="88"/>
        <source>Table of Contents</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="89"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="92"/>
        <source>&amp;Print page</source>
        <translation>&amp;Imprimer la page</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="131"/>
        <source>Print page</source>
        <translation>Imprimer la page</translation>
    </message>
</context>
<context>
    <name>IllustrationDialog</name>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="45"/>
        <source>Introduction</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="76"/>
        <source>Welcome to TIPP10</source>
        <translation>Bienvenue à TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="81"/>
        <source>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</source>
        <translation>TIPP10 est un tuteur de dactylographie gratuit pour Windows, Mac OS et Linux. La chose ingénieuse à propos du logiciel est sa fonction d&apos;intelligence. Les caractères mal saisis sont répétés plus fréquemment. La saisie tactile n&apos;a jamais été aussi facile à apprendre.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="88"/>
        <source>Tips for using the 10 finger system</source>
        <translation>Conseils pour utiliser le système à 10 doigts</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="92"/>
        <source>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</source>
        <translation>1. Placez d&apos;abord vos doigts dans la position initiale (elle est affichée au début de chaque leçon). Les doigts reviennent à la ligne d&apos;accueil après chaque pression sur une touche.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="98"/>
        <source>en</source>
        <translation>fr</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="102"/>
        <source>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</source>
        <translation>2. Assurez-vous que votre posture est droite et évitez de regarder le clavier. Vos yeux doivent être dirigés vers le moniteur à tout moment.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="107"/>
        <source>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</source>
        <translation>3. Amenez vos bras sur le côté de votre corps et détendez vos épaules. Votre avant-bras et votre avant-bras doivent former un angle droit. Ne reposez pas vos poignets et restez en position verticale.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="113"/>
        <source>4. Try to remain relaxed during the typing lessons.</source>
        <translation>4. Essayez de rester détendu pendant les cours de dactylographie.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="116"/>
        <source>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</source>
        <translation>5. Essayez de réduire au minimum les erreurs de frappe. Il est beaucoup moins efficace de taper rapidement si vous faites beaucoup d&apos;erreurs.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="120"/>
        <source>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</source>
        <translation>6. Une fois que vous avez commencé la saisie tactile, vous devez éviter de revenir à la façon dont vous aviez l&apos;habitude de taper (même si vous êtes pressé).</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="124"/>
        <source>If you need assistance with using the software use the help function.</source>
        <translation>Si vous avez besoin d&apos;aide pour utiliser le logiciel, utilisez la fonction d&apos;aide.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="135"/>
        <source>All rights reserved.</source>
        <translation>Tous les droits sont réservés.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="149"/>
        <source>&amp;Launch TIPP10</source>
        <translation>&amp;Démarrer TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="153"/>
        <source>Do&amp;n&apos;t show me this window again</source>
        <translation>&amp;Ne plus afficher</translation>
    </message>
</context>
<context>
    <name>LessonDialog</name>
    <message>
        <location filename="../widget/lessondialog.cpp" line="119"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="120"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauver</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="121"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="129"/>
        <source>Name of the lesson (20 characters max.):</source>
        <translation>Nom de la leçon (20 caractères max.) :</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="131"/>
        <source>Short description (120 characters max.):</source>
        <translation>Petite description (120 caractères max.) :</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="132"/>
        <source>Lesson content (at least two lines):</source>
        <translation>Contenu de la leçon (au moins 2 lignes):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="134"/>
        <source>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</source>
        <translation>&lt;u&gt;Explication :&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Chaque ligne est équivalente à une unité de leçon. Il y a deux façons de dicter une leçon :&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Leçon de phrases&lt;/b&gt; - chaque ligne (phrase) sera dictée jusqu&apos;à la fin.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Leçon de mots&lt;/b&gt; - Les mots seront dictés les uns à la suite des autres mais contrairement au précédent mode, le retour à la ligne se fera après %1 caractères.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="144"/>
        <source>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</source>
        <translation>&lt;b&gt;Que se passe-t-il quand le mode Intelligent est actif ?&lt;/b&gt;&lt;br&gt;Les lignes dictées seront sélectionnées en fonction de vos erreurs de frappe. Activer ce mode est possible uniquement pour les leçons comportant beaucoup de lignes (souvent le cas des leçons de mots).&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="152"/>
        <source>Dictate the text as:</source>
        <translation>Type de leçon :</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="163"/>
        <source>Sentence Lesson</source>
        <translation>Leçon de phrases</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="165"/>
        <source>Every line will be completed with
a line break at the end</source>
        <translation>Chaque ligne sera dictée
avec un retour chariot à la fin</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="168"/>
        <source>Word Lesson</source>
        <translation>Leçon de mots</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="170"/>
        <source>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</source>
        <translation>Every unit (line) will be seperated 
by blanks. A line break passes 
auomatically after at least %1 caractères.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="177"/>
        <source>Edit own Lesson</source>
        <translation>Modifier la leçon personnelle</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="179"/>
        <source>Name of the Lesson:</source>
        <translation>Nom de la leçon :</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="183"/>
        <source>Create own Lesson</source>
        <translation>Créer une leçon personnelle</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="243"/>
        <source>Please enter the name of the lesson
</source>
        <translation>Veuillez entrer le nom de la leçon
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="249"/>
        <source>Please enter entire lesson content
</source>
        <translation>Veuillez entrer le contenu de la leçon
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="266"/>
        <source>Please enter at least two lines of text
</source>
        <translation>Veuillez entrer au moins deux lignes
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="272"/>
        <source>Please enter 400 lines of text maximum
</source>
        <translation>Veuillez entrer 400 lignes maximum
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="280"/>
        <source>The name of the lesson already exists. Please enter a new lesson name.
</source>
        <translation>Ce nom de leçon existe déjà. Veuillez en saisir un autre.
</translation>
    </message>
</context>
<context>
    <name>LessonPrintDialog</name>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="39"/>
        <source>Print Lesson</source>
        <translation>Imprimer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="61"/>
        <source>&amp;Print</source>
        <translation>&amp;Imprimer</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="62"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="75"/>
        <source>Please enter your name:</source>
        <translation>Veuillez entre votre nom :</translation>
    </message>
</context>
<context>
    <name>LessonResult</name>
    <message>
        <location filename="../widget/lessonresult.cpp" line="65"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="87"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="92"/>
        <source>%n character(s)</source>
        <translation>
            <numerusform>%n character</numerusform>
            <numerusform>%n characters</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="96"/>
        <source>Entire Lesson</source>
        <translation>Leçon complète</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="103"/>
        <source>Error Correction with Backspace</source>
        <translation>Correction d&apos;erreur avec retour arrière</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="106"/>
        <source>Error Correction without Backspace</source>
        <translation>Correction d&apos;erreur sans retour arrière</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="108"/>
        <source>Ignore Errors</source>
        <translation>Ignorer les erreurs</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="116"/>
        <source>None</source>
        <translation>Aucune</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="124"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="132"/>
        <source>- Colored Keys</source>
        <translation>- touches colorées</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="138"/>
        <source>- Home Row</source>
        <translation>- indication des doigts sur la rangée du milieu</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="144"/>
        <source>- Motion Paths</source>
        <translation>- trait de déplacement des doigts</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="150"/>
        <source>- Separation Line</source>
        <translation>- ligne de séparation</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="156"/>
        <source>- Instructions</source>
        <translation>- indication des doigts à utiliser</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="188"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="191"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="201"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="267"/>
        <location filename="../widget/lessonresult.cpp" line="462"/>
        <source>You have reached %1 at a typing speed of %2 cpm and %n typing error(s).</source>
        <translation>
            <numerusform>Vous avez atteint %1 à une vitesse de frappe de %2 cpm et %n erreur de frappe.</numerusform>
            <numerusform>Vous avez atteint %1 à une vitesse de frappe de %2 cpm et %n erreurs de frappe.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="285"/>
        <location filename="../widget/lessonresult.cpp" line="480"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="296"/>
        <location filename="../widget/lessonresult.cpp" line="343"/>
        <location filename="../widget/lessonresult.cpp" line="492"/>
        <location filename="../widget/lessonresult.cpp" line="538"/>
        <source>Duration: </source>
        <translation>Durée : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="300"/>
        <location filename="../widget/lessonresult.cpp" line="496"/>
        <source>Typing Errors: </source>
        <translation>Erreurs de frappe : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="304"/>
        <location filename="../widget/lessonresult.cpp" line="500"/>
        <source>Assistance: </source>
        <translation>Assistance: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="318"/>
        <location filename="../widget/lessonresult.cpp" line="513"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="335"/>
        <location filename="../widget/lessonresult.cpp" line="530"/>
        <source>Lesson: </source>
        <translation>Leçon: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="339"/>
        <location filename="../widget/lessonresult.cpp" line="534"/>
        <source>Time: </source>
        <translation>Temps : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="347"/>
        <location filename="../widget/lessonresult.cpp" line="542"/>
        <source>Characters: </source>
        <translation>Caractères : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="351"/>
        <location filename="../widget/lessonresult.cpp" line="546"/>
        <source>Errors: </source>
        <translation>Erreurs : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="355"/>
        <location filename="../widget/lessonresult.cpp" line="550"/>
        <source>Error Rate: </source>
        <translation>Taux d&apos;erreur: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="359"/>
        <location filename="../widget/lessonresult.cpp" line="554"/>
        <source>Cpm: </source>
        <translation>CPM : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="373"/>
        <location filename="../widget/lessonresult.cpp" line="567"/>
        <source>Dictation</source>
        <translation>Dictée</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="456"/>
        <source>TIPP10 Touch Typing Tutor</source>
        <translation>Tuteur de dactylographie TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="459"/>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="459"/>
        <source> of %1</source>
        <translation> de %1</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="584"/>
        <source>Print Report</source>
        <translation>Imprimer le rapport</translation>
    </message>
</context>
<context>
    <name>LessonSqlModel</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="84"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="88"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="92"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
    <message numerus="yes">
        <location filename="../sql/lessontablesql.cpp" line="104"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonTableSql</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="140"/>
        <source>Show: </source>
        <translation>Afficher : </translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="142"/>
        <source>All Lessons</source>
        <translation>Toutes les leçons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="143"/>
        <source>Training Lessons</source>
        <translation>Leçons d&apos;entraînement</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="144"/>
        <source>Open Lessons</source>
        <translation>Leçons ouvertes</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="145"/>
        <source>Own Lessons</source>
        <translation>Leçons personnelles</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="287"/>
        <source>Lesson</source>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="288"/>
        <source>Time</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="289"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="290"/>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="291"/>
        <source>Errors</source>
        <translation>Erreurs</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="292"/>
        <source>Rate</source>
        <translation>fréquence</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="293"/>
        <source>Cpm</source>
        <translation>CPM</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="294"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="297"/>
        <source>This column shows the names
of completed lessons</source>
        <translation>Cette colonne affiche le nom
des leçons complétées</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="301"/>
        <source>Start time of the lesson</source>
        <translation>Début de la leçon</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="303"/>
        <source>Total duration of the lesson</source>
        <translation>Durée totale de la leçon</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="304"/>
        <source>Number of characters dictated</source>
        <translation>Nombre de caractères dictés</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="307"/>
        <source>Number of typing errors</source>
        <translation>Nombre d&apos;erreurs de frappe</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="309"/>
        <source>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</source>
        <translation>Le taux d&apos;erreur est calculé comme suit :
Erreurs / Caractères
Plus le taux d&apos;erreur est faible, mieux c&apos;est !</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="314"/>
        <source>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</source>
        <translation>&quot;CPM&quot; indique le nombre moyen de caractères tapés par minute</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="318"/>
        <source>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</source>
        <translation>Votre classement est calculé ainsi :
((caractères - (20 x erreurs) / durée en minutes ) x 0.4

Notez que tapez lentement sans erreur permet un meilleur classement
qu&apos;une frappe rapide avec beaucoup d&apos;erreurs !</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../widget/mainwindow.cpp" line="76"/>
        <source>All results of the current lesson will be discarded!

Do you really want to exit?

</source>
        <translation>Tous les résultats de la leçon courante seront perdus

Voulez-vous vraiment quitter ?

</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="97"/>
        <location filename="../widget/mainwindow.cpp" line="115"/>
        <source>&amp;Go</source>
        <translation>&amp;Aller à</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="103"/>
        <location filename="../widget/mainwindow.cpp" line="119"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="111"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="130"/>
        <source>&amp;Settings</source>
        <translation>&amp;Paramètres</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="131"/>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="133"/>
        <source>&amp;Results</source>
        <translation>&amp;Résultats</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="134"/>
        <source>&amp;Manual</source>
        <translation>&amp;Manuel</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="136"/>
        <source> on the web</source>
        <translation> sur Internet</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="138"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="140"/>
        <source>&amp;About </source>
        <translation>&amp;A propos </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="143"/>
        <source>ABC-Game</source>
        <translation>Jeu ABC</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="196"/>
        <source>Software Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="197"/>
        <source>Database Version </source>
        <translation>Version de la base de données </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="202"/>
        <source>Intelligent Touch Typing Tutor</source>
        <translation>Tuteur de dactylographie intelligente</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="203"/>
        <source>On the web: </source>
        <translation>Sur le Web: </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="205"/>
        <source>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</source>
        <translation>TIPP10 est publié sous la licence publique générale GNU et est disponible gratuitement. Vous n&apos;avez pas à payer pour cela où que vous le téléchargiez !</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="215"/>
        <source>About </source>
        <translation>A propos </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="199"/>
        <source>Portable Version</source>
        <translation>Version portable</translation>
    </message>
</context>
<context>
    <name>ProgressionWidget</name>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="48"/>
        <location filename="../widget/progressionwidget.cpp" line="62"/>
        <location filename="../widget/progressionwidget.cpp" line="530"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="54"/>
        <source>Show: </source>
        <translation>Afficher : </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="56"/>
        <source>All Lessons</source>
        <translation>Toutes les leçons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="57"/>
        <source>Training Lessons</source>
        <translation>Exercices</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="58"/>
        <source>Open Lessons</source>
        <translation>Leçons libres</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="59"/>
        <source>Own Lessons</source>
        <translation>Leçons personnelles</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="60"/>
        <source>Order by x-axis:</source>
        <translation>Ordre des abscisses :</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="63"/>
        <location filename="../widget/progressionwidget.cpp" line="536"/>
        <source>Lesson</source>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="241"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="384"/>
        <location filename="../widget/progressionwidget.cpp" line="408"/>
        <source>Training Lesson</source>
        <translation>Exercice</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="391"/>
        <location filename="../widget/progressionwidget.cpp" line="415"/>
        <source>Open Lesson</source>
        <translation>Leçon libre</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="398"/>
        <location filename="../widget/progressionwidget.cpp" line="422"/>
        <source>Own Lesson</source>
        <translation>Leçon personnelle</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/progressionwidget.cpp" line="481"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="482"/>
        <source>%1 cpm</source>
        <translation>%1 CPM</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="499"/>
        <source>The progress graph will be shown after completing two lessons.</source>
        <translation>Les statistiques de progression seront affichées après avoir effectué deux leçons.</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="541"/>
        <source>Cpm</source>
        <translation>CPM</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="546"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sql/connection.h" line="111"/>
        <location filename="../sql/connection.h" line="119"/>
        <location filename="../sql/connection.h" line="139"/>
        <source>Affected directory:
</source>
        <translation>Chemin :
</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="47"/>
        <location filename="../widget/fingerwidget.cpp" line="131"/>
        <source>Left little finger</source>
        <translation>Auriculaire gauche</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="48"/>
        <location filename="../widget/fingerwidget.cpp" line="132"/>
        <source>Left ring finger</source>
        <translation>Annulaire gauche</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="49"/>
        <location filename="../widget/fingerwidget.cpp" line="133"/>
        <source>Left middle finger</source>
        <translation>Majeur gauche</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="50"/>
        <location filename="../widget/fingerwidget.cpp" line="134"/>
        <source>Left forefinger</source>
        <translation>Index gauche</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="50"/>
        <location filename="../widget/fingerwidget.cpp" line="135"/>
        <source>Right forefinger</source>
        <translation>Index droit</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="51"/>
        <location filename="../widget/fingerwidget.cpp" line="136"/>
        <source>Right middle finger</source>
        <translation>Majeur droit</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="52"/>
        <location filename="../widget/fingerwidget.cpp" line="137"/>
        <source>Right ring finger</source>
        <translation>Annulaire droit</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="53"/>
        <location filename="../widget/fingerwidget.cpp" line="138"/>
        <source>Right little finger</source>
        <translation>Auriculaire droit</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="53"/>
        <location filename="../widget/fingerwidget.cpp" line="138"/>
        <source>Thumb</source>
        <translation>Pouce</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="101"/>
        <source>en</source>
        <translation>fr</translation>
    </message>
</context>
<context>
    <name>RegExpDialog</name>
    <message>
        <location filename="../widget/regexpdialog.ui" line="14"/>
        <source>Filter for the keyboard layout</source>
        <translation>Filtre pour la disposition du clavier</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="20"/>
        <source>Limitation of characters</source>
        <translation>Limitation des caractères</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="36"/>
        <source>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</source>
        <translation>Vous devriez essayer d&apos;éviter d&apos;utiliser des caractères non pris en charge par la disposition de votre clavier. Un filtre en tant qu&apos;expression régulière est appliqué à tous les textes d&apos;entraînement avant le début de la leçon. Vous ne devez apporter des modifications ici que si vous êtes familiarisé avec les expressions régulières.</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="46"/>
        <source>Replacement Filter</source>
        <translation>Filtre de remplacement</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="61"/>
        <source>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</source>
        <translation>Le filtrage des caractères non autorisés peut produire des textes qui n&apos;ont pas beaucoup de sens (par exemple, en supprimant les trémas). Vous pouvez définir des remplacements qui seront appliqués avant que la limitation de caractères ne soit appliquée. Veuillez suivre l&apos;exemple ici qui remplace tous les trémas allemands et le symbole ß :
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../widget/settings.cpp" line="78"/>
        <source>Some of your settings require a restart of the software to take effect.
</source>
        <translation>Certains de vos paramètres nécessitent un redémarrage du logiciel pour prendre effet.</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="295"/>
        <source>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</source>
        <translation>Toutes les indicateurs d&apos;achèvement des leçons vont être supprimés
ainsi que l&apos;historique des leçons effectuées!

Voulez-vous continuer?

</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="309"/>
        <source>The data for completed lessons was successfully deleted!
</source>
        <translation>Les indicateurs d&apos;achèvement des leçons ont été supprimées avec succès!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="320"/>
        <source>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</source>
        <translation>Toutes les statistiques des caractères tapés vont être supprimés!

Voulez-vous continuer?</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="334"/>
        <source>The recorded characters were successfully deleted!
</source>
        <translation>Les statistiques des caractères tapés ont été supprimées avec succès!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="20"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="42"/>
        <source>Training</source>
        <translation>Entraînement</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="48"/>
        <source>Ticker</source>
        <translation>Téléscripteur</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="62"/>
        <source>Here you can select the background color</source>
        <translation>Ici, vous pouvez sélectionner la couleur de fond</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="78"/>
        <source>Here you can select the font color</source>
        <translation>Ici, vous pouvez sélectionner la couleur de la police</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="88"/>
        <source>Cursor:</source>
        <translation>Le curseur:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="101"/>
        <source>Here you can change the font of the ticker (a font size larger than 20 pt is not recommended)</source>
        <translation>Ici vous pouvez changer la police de caractères du téléscripteur
(une taille supérieure à 20 points n&apos;est pas recommandée)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="104"/>
        <source>Change &amp;Font</source>
        <translation>&amp;Changer la police</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="117"/>
        <source>Here can select the color of the cursor for the current character</source>
        <translation>Ici peut sélectionner la couleur du curseur pour le caractère actuel</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="133"/>
        <source>Font:</source>
        <translation>Police:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="140"/>
        <source>Font Color:</source>
        <translation>Couleur de la police:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="147"/>
        <source>Background:</source>
        <translation>Fond:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="164"/>
        <source>Speed:</source>
        <translation>Vitesse:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="179"/>
        <source>Off</source>
        <translation>Lente</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="192"/>
        <source>Here you can change the speed of the ticker (Slider on the left: Ticker does not move until reaching the end of line. Slider on the right: The ticker moves very fast.)</source>
        <translation>Ici vous pouvez régler la vitesse du téléscripteur
(curseur à gauche: le téléscripteur ne défilera pas jusqu&apos;à la fin de la ligne.
curseur à droite: le téléscripteur dévilera très rapidement.)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="214"/>
        <source>Fast</source>
        <translation>Rapide</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="228"/>
        <source>Audio Output</source>
        <translation>Sortie audio</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="234"/>
        <source>Here you can activate a metronome</source>
        <translation>Ici vous pouvez activer un métronome</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="237"/>
        <source>Metronome:</source>
        <translation>Métronome:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="266"/>
        <source>Select how often the metronome sound should appear per minute</source>
        <translation>Sélectionnez la fréquence à laquelle le son du métronome doit apparaître par minute</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="272"/>
        <source> cpm</source>
        <translation> CPM</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="292"/>
        <location filename="../widget/settings.ui" line="298"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="317"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="324"/>
        <source>Training Lessons:</source>
        <translation>Exercices:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="342"/>
        <source>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</source>
        <translation>Les leçons de formation que vous avez choisies ne sont pas adaptées à la disposition du clavier. Vous pouvez continuer mais vous devrez peut-être mettre de côté certaines clés depuis le début.</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="352"/>
        <source>Keyboard Layout:</source>
        <translation>Disposition de clavier:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="376"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="382"/>
        <source>User Data</source>
        <translation>Données utilisateur</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="388"/>
        <source>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation</source>
        <translation>Ici vous pouvez effacer les indicateurs d&apos;achèvement des leçons ainsi que l&apos;historique des leçons effectuées</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="391"/>
        <source>Reset &amp;completed lessons</source>
        <translation>Effacer les &amp;indicateurs de leçons achevées</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="398"/>
        <source>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</source>
        <translation>Ici vous pouvez effacer les statistiques des caractères tapés</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="401"/>
        <source>Reset all &amp;recorded characters</source>
        <translation>Effacer les statistiques des &amp;caractères tapés</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="412"/>
        <source>Other</source>
        <translation>Autre</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="418"/>
        <source>Windows</source>
        <translation>Windows</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="424"/>
        <source>Here you can decide if an information window with tips is shown at the beginning</source>
        <translation>Ici vous pouvez choisir d&apos;afficher une fenêtre d&apos;informations
au démarrage</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="427"/>
        <source>Show welcome message at startup</source>
        <translation>Afficher le message de bienvenue au démarrage</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="434"/>
        <source>Here you can define whether a warning window is displayed when an open or own lesson with active intelligence is started</source>
        <translation>Ici vous pouvez choisir d&apos;afficher un avertissement
lorsqu&apos;une leçon en mode intelligent commence</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="437"/>
        <source>Remember enabled intelligence before starting
an open or own lesson</source>
        <translation>Sauver le mode intelligent avant
de commencer une leçon</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="451"/>
        <source>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</source>
        <translation>Sélectionner la durée complète de la leçon
à la désactivation du mode intelligent</translation>
    </message>
</context>
<context>
    <name>StartWidget</name>
    <message>
        <location filename="../widget/startwidget.cpp" line="105"/>
        <location filename="../widget/startwidget.cpp" line="753"/>
        <source>Training Lessons</source>
        <translation>Leçons d&apos;entraînement</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="112"/>
        <source>Subject:</source>
        <translation>Thème:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="164"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="168"/>
        <source>&amp;New Lesson</source>
        <translation>&amp;Nouvelle leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="170"/>
        <source>&amp;Import Lesson</source>
        <translation>&amp;Importer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="172"/>
        <source>&amp;Export Lesson</source>
        <translation>&amp;Exporter la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="174"/>
        <source>&amp;Edit Lesson</source>
        <translation>&amp;Editer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="176"/>
        <source>&amp;Delete Lesson</source>
        <translation>&amp;Supprimer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="205"/>
        <source>Duration of Lesson</source>
        <translation>Durée de la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="208"/>
        <source>Time Limit:</source>
        <translation>Limite de temps:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="210"/>
        <location filename="../widget/startwidget.cpp" line="217"/>
        <source>The dictation will be stopped after
a specified time period</source>
        <translation>La dictée sera arrêtée après
un temps donné</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="216"/>
        <source> minutes</source>
        <translation> minutes</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="221"/>
        <source>Character Limit:</source>
        <translation>Limite des caractères:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="231"/>
        <source> characters</source>
        <translation> caractères</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="223"/>
        <location filename="../widget/startwidget.cpp" line="233"/>
        <source>The dictation will be stopped after
a specified number of correctly typed
characters</source>
        <translation>La dictée sera arrêté après
un certain nombre de caractères
tapés</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="237"/>
        <source>Entire
Lesson</source>
        <translation>Leçon complète</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="239"/>
        <source>The complete lesson will be dictated
from beginning to end</source>
        <translation>La leçon sera dictée dans
sa globalité</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="240"/>
        <source>(Entire Lesson)</source>
        <translation>(leçon entière)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="270"/>
        <source>Response to Typing Errors</source>
        <translation>Prise en charge des erreurs de frappe</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="273"/>
        <source>Block Typing Errors</source>
        <translation>Arrêter le téléscripteur</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="275"/>
        <source>The dictation will only proceed if the correct
key was pressed</source>
        <translation>La dictée continuera uniquement si
la bonne touche est pressée</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="278"/>
        <source>Correction with Backspace</source>
        <translation>Correction des erreurs</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="280"/>
        <source>Typing errors have to be removed
with the return key</source>
        <translation>Les erreurs de frappe doivent être
corrigées avec la touche Backspace</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="283"/>
        <source>Audible Signal</source>
        <translation>Signal acoustique</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="284"/>
        <source>A beep sounds with every typing error</source>
        <translation>Emettre un bip à chaque erreur de frappe</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="287"/>
        <source>Show key picture</source>
        <translation>Afficher l&apos;image clé</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="288"/>
        <source>For every typing error the corresponding key picture is displayed on the keyboard</source>
        <translation>Pour chaque faute de frappe, l&apos;image clé correspondante s&apos;affiche sur le clavier</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="294"/>
        <source>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>* Le texte de la leçon sera dicté en fonction de vos erreurs de frappe en temps réel.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="300"/>
        <source>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>* Sélectionnez cette option afin d&apos;adapter l&apos;ordre de diction des mots de la dictée à vos erreurs en temps réel.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="307"/>
        <source>Intelligence</source>
        <translation>Mode intelligent</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="309"/>
        <source>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</source>
        <translation>Les mots et les phrases de la leçon seront dictés 
en fonction de vos erreurs de frappe en temps réel.
Cela est valable uniquement si le mode intelligent est sélectionné.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="341"/>
        <source>Assistance</source>
        <translation>Assistance</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="343"/>
        <source>Show Keyboard</source>
        <translation>Afficher le clavier virtuel</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="344"/>
        <source>For visual support, the virtual keyboard and
status information is shown</source>
        <translation>Pour un support visuel, le clavier virtuel et
les informations de statut seront affichées</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="348"/>
        <source>Colored Keys</source>
        <translation>Touches Colorées</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="350"/>
        <source>For visual support pressing keys will be
marked with colors</source>
        <translation>Pour un support visuel les touches 
à taper seront colorées</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="353"/>
        <source>Home Row</source>
        <translation>Ligne d&apos;accueil</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="355"/>
        <source>For visual support, the remaining fingers
of the home row will be colored</source>
        <translation>Pour un support visuel les doigts de la rangée
du milieu seront colorés</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="359"/>
        <source>L/R Separation Line</source>
        <translation>Ligne de séparation gauche/droite</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="361"/>
        <source>For visual support a separation line between left
and right hand will be shown</source>
        <translation>Pour un support visuel une ligne de séparation
entre la main droite et la main gauche sera affichée</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="365"/>
        <source>Instruction</source>
        <translation>Instruction</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="367"/>
        <source>Show fingers to be used in the status bar</source>
        <translation>Les doigts à utiliser seront affichés dans la barre de statut</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="370"/>
        <source>Motion Paths</source>
        <translation>Chemins de mouvement</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="372"/>
        <source>Motion paths of the fingers will be shown
on the keyboard</source>
        <translation>Les déplacements de doigts seront affichés
sur le clavier</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="419"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="420"/>
        <source>&amp;Start Training</source>
        <translation>&amp;Démarrer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="546"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="765"/>
        <source>Open Lessons</source>
        <translation>Leçons ouvertes</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="795"/>
        <source>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</source>
        <translation>Pour le moment, les cours ouverts n&apos;existent qu&apos;en langue allemande. Nous espérons proposer bientôt des cours ouverts en anglais.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="804"/>
        <source>Own Lessons</source>
        <translation>Leçons personnelles</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="928"/>
        <source>Please select a text file</source>
        <translation>Veuillez sélectionner un fichier texte</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="929"/>
        <source>Text files (*.txt)</source>
        <translation>Fichiers textes (*.txt)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1051"/>
        <source>Please indicate the location of a text file</source>
        <translation>Veuillez indiquer l&apos;emplacement d&apos;un fichier texte</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1100"/>
        <source>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</source>
        <translation>Voulez-vous vraiment supprimer la leçon
and toutes les statistiques enregistrées pour
celle-ci?</translation>
    </message>
</context>
<context>
    <name>TickerBoard</name>
    <message>
        <location filename="../widget/tickerboard.h" line="133"/>
        <source>Press space bar to proceed</source>
        <translation>Tapez Espace pour démarrer</translation>
    </message>
    <message>
        <location filename="../widget/tickerboard.cpp" line="151"/>
        <source>Dictation Finished</source>
        <translation>Dictée terminée</translation>
    </message>
</context>
<context>
    <name>TrainingWidget</name>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="120"/>
        <source>&amp;Pause</source>
        <translatorcomment>&amp;Pause</translatorcomment>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="127"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="133"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="234"/>
        <source>Press space bar to start</source>
        <translation>Appuyez sur la touche Espace pour commencer</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="235"/>
        <source>Take Home Row position</source>
        <translation>Position du mileu</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="251"/>
        <source>Do you really want to exit the lesson early?</source>
        <translation>Voulez-vous vraiment quitter la leçon ?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="265"/>
        <source>Do you want to save your results?</source>
        <translation>Voulez-vous sauver vos résultats ?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="481"/>
        <source>E&amp;xit Lesson early</source>
        <translation>&amp;Quitter la leçon</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="534"/>
        <source>Errors: </source>
        <translation>Erreurs : </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="536"/>
        <source>Cpm: </source>
        <translation>CPM : </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="537"/>
        <source>Time: </source>
        <translation>Temps : </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="539"/>
        <source>Characters: </source>
        <translation>Caractères : </translation>
    </message>
</context>
</TS>
