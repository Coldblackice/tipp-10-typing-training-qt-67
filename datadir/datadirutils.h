#ifndef DATADIRUTILS_H
#define DATADIRUTILS_H

#include <QObject>
#include <QString>

class DataDirUtils : public QObject {
    Q_OBJECT

public:
    static QString getDataDir();

    static void setDataDir(QString dir);

private:
    static QString datadir;
};

#endif // DATADIRUTILS_H