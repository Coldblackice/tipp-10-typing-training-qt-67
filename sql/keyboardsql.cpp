/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Implementation of the KeyboardSql class
** File name: keyboardsql.cpp
**
****************************************************************/

#include <QFile>
#include <QSqlQuery>
#include <QString>
#include <QTextStream>
#include <QVariant>

#include "keyboardsql.h"

KeyboardSql::KeyboardSql(QString keyboardLayout)
    : layout(keyboardLayout)
{
    // Get finger text
    getFingerDescription();
}

void KeyboardSql::getFingerDescription()
{
    fingers << QObject::tr("Left little finger")
            << QObject::tr("Left ring finger")
            << QObject::tr("Left middle finger")
            << QObject::tr("Left forefinger") << QObject::tr("Right forefinger")
            << QObject::tr("Right middle finger")
            << QObject::tr("Right ring finger")
            << QObject::tr("Right little finger") << QObject::tr("Thumb");
}

bool KeyboardSql::getKeyLayout(QChar givenchar, int* left, int* top, int* color,
    int* form, int* modifier, int* modifier2, int* finger, QString* status)
{

    // Convert given char into properties of a key
    QString tableName = "keyboard_grids";

    QSqlQuery query;
    if (query.exec("SELECT " + tableName + ".left, " + tableName + ".top, "
            + tableName + ".color, " + tableName + ".form, "
            + "keyboard_layouts.modifier1, " + "keyboard_layouts.modifier2, "
            + tableName + ".finger " + "FROM keyboard_layouts, " + tableName
            + " " + "WHERE keyboard_layouts.layout = '" + layout + "' " + "AND "
            + tableName + ".layout_country = '" + layout.left(2) + "' " + "AND "
            + tableName + ".key = keyboard_layouts.grid "
            + "AND keyboard_layouts.unicode = "
            + QString::number(givenchar.unicode()) + ";")) {
        if (query.first()) {
            *left = query.value(0).toInt();
            *top = query.value(1).toInt();
            *color = query.value(2).toInt();
            *form = query.value(3).toInt();
            *modifier = query.value(4).toInt();
            *modifier2 = query.value(5).toInt();
            *finger = query.value(6).toInt();
            *status = fingers.at(query.value(6).toInt());
            return true;
        }
    }
    return false;
}

bool KeyboardSql::getModifierLayout(int givenmodifier, int* left, int* top,
    int* color, int* form, int* finger, QString* status)
{

    // Convert given id into properties of a key
    QString tableName = "keyboard_grids";

    QSqlQuery query;
    if (query.exec("SELECT " + tableName + ".left, " + tableName + ".top, "
            + tableName + ".color, " + tableName + ".form, " + tableName
            + ".finger FROM " + tableName + " WHERE " + tableName
            + ".layout_country = '" + layout.left(2) + "' " + "AND " + tableName
            + ".key = " + QString::number(givenmodifier) + ";")) {
        if (query.first()) {
            *left = query.value(0).toInt();
            *top = query.value(1).toInt();
            *color = query.value(2).toInt();
            *form = query.value(3).toInt();
            *finger = query.value(4).toInt();
            status->prepend(fingers.at(query.value(4).toInt()) + " + ");
            return true;
        }
    }
    return false;
}

bool KeyboardSql::getNumLayout(QChar givenchar, int* left, int* top, int* color,
    int* form, int* finger, QString* status)
{

    // Convert given char into properties of a key
    QString tableName = "numboard_grids";

    QSqlQuery query;
    if (query.exec("SELECT " + tableName + ".left, " + tableName
            + ".top, "
              ""
            + tableName + ".color, " + tableName + ".form, " + tableName
            + ".finger "
              "FROM numboard_layouts, "
            + tableName + " " + "WHERE numboard_layouts.layout_country = '"
            + layout.left(2) + "' " + "AND numboard_layouts.layout_os = '"
            + layout.right(3) + "' " + "AND " + tableName
            + ".layout_country = '" + layout.left(2) + "' " + "AND " + tableName
            + ".layout_os = '" + layout.right(3) + "' " + "AND " + tableName
            + ".key = numboard_layouts.grid "
            + "AND numboard_layouts.unicode = "
            + QString::number(givenchar.unicode()) + ";")) {
        if (query.first()) {
            *left = query.value(0).toInt();
            *top = query.value(1).toInt();
            *color = query.value(2).toInt();
            *form = query.value(3).toInt();
            *finger = query.value(4).toInt();
            *status = fingers.at(query.value(4).toInt());
            return true;
        }
    }
    return false;
}
